package com.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.entity.User;

public class SelectTest {
EntityManagerFactory factory = null;
EntityManager em = null;
@BeforeAll
public void setup() {
factory = Persistence.createEntityManagerFactory("unit1");
em = factory.createEntityManager();
}
@Test
public void selectTest() {
User p = em.find(User.class, 116);

 assertEquals("monitor", p.getpName());
assertEquals(13000, p.getPrice());
assertEquals(5, p.getQuantity());
}
@AfterAll
public void close() {
em.close();
factory.close();
}
}