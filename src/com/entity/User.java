package com.entity;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
@Id
private int uid;
private String uName;
private int price;
private int quantity;
public User() {
super();
}
public User(int uid, String uName, int price, int quantity) {
super();
this.uid = uid;
this.uName = uName;
this.price = price;
this.quantity = quantity;
}
public int getPid() {
return uid;
}
public void setPid(int uid) {
this.uid = uid;
}
public String getpName() {
return uName;
}
public void setpName(String uName) {
this.uName = uName;
}
public int getPrice() {
return price;
}
public void setPrice(int price) {
this.price = price;
}
public int getQuantity() {
return quantity;
}
public void setQuantity(int quantity) {
this.quantity = quantity;
}
@Override
public String toString() {
return "Product [pid=" + uid + ", pName=" + uName + ", price=" + price + ", quantity=" + quantity + "]";
}

}