package com.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.entity.User;

public class Insert {
public static void main(String[] args) {
//we will talk to database
EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1"); //load database configuration, open database
EntityManager em = factory.createEntityManager(); //rest of the operations will be handled by entitymanager
em.getTransaction().begin();
User p = new User(103, "printer", 12000, 8);
User p1 = new User(104, "keyboard", 2400, 9);
User p2 = new User(105, "mouse", 600, 10);
em.persist(p); //insert into product values (103, 'printer', 12000, 8);
em.persist(p1); //insert into product values (104, 'keyboard, 2400, 9);
em.persist(p2); //insert into product values (105,'mouse', 600, 10);
em.getTransaction().commit();
em.close();
factory.close();
}
}